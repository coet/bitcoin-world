# source:
# https://counterpartytalk.org/t/is-it-possible-to-create-list-of-addresses-and-private-keys-from-counterwallet-pass-phrase/1223

import argparse
import json
import math
from binascii import unhexlify

from bip32utils.BIP32Key import *
from ecdsa.ecdsa import int_to_string


class UnknownAddressException(Exception):
    def __init__(self, exception):
        """Unknown Address."""
        print(exception)


class InvalidMnemonicException(Exception):
    def __init__(self, exception):
        """User has passed unknown word."""
        print(exception)


class CounterWalletHelper:
    def __init__(self, args):
        self.mnemonic_word_list = json.load(open("resources/bip32.json", 'r'))
        self._b58chars = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
        # both actions
        self.old = args.old
        self.show_private = args.show_private
        self.depth = abs(args.search_depth)
        # recover
        self.pass_phrase = self.split_phrase(args.pass_phrase) if hasattr(args, 'pass_phrase') else None
        # wallet
        self.known_phrase = self.split_phrase(args.known_phrase) if hasattr(args, 'known_phrase') else None
        self.known_address = args.known_address if hasattr(args, 'known_address') else None
        self.insertion_index = args.insertion_index if hasattr(args, 'insertion_index') else None

    def make_seed(self, mnemonic_word_list=None):
        if not mnemonic_word_list:
            mnemonic_word_list = self.mnemonic_word_list
        n = len(mnemonic_word_list)
        s = []
        for i in range(4):  # len(phrase) / 3
            w1 = mnemonic_word_list.index(self.pass_phrase[3 * i])
            w2 = mnemonic_word_list.index(self.pass_phrase[3 * i + 1])
            w3 = mnemonic_word_list.index(self.pass_phrase[3 * i + 2])
            s.append(w1 + n * self.mMod(w2 - w1, n) + n * n * self.mMod(w3 - w2, n))
        return s

    @staticmethod
    def mMod(a, b):
        return int(a - math.floor(a / b) * b)

    @staticmethod
    def to_hex(s):
        seed = ''
        for i in s:
            seed += ('0' * 8 + format(i, 'x'))[-8:]
        return seed

    @staticmethod
    def bytes_to_words(seed):
        words = {}
        for i in range(32):
            ps = (i * 8) >> 5
            try:
                ls = words[ps]
            except KeyError:
                ls = 0
            try:
                rs = int(seed[i]) << (24 - (i % 4) * 8)
            except:
                rs = 0
            words[ps] = (ls | rs)
        return words

    @staticmethod
    def to_word_array(words):
        b = b''
        for i in words:
            b += struct.pack('>i', words[i])
        return b

    @staticmethod
    def create_wallet(entropy):
        I = hmac.new(b"Bitcoin seed", entropy, hashlib.sha512).digest()
        Il, Ir = I[:32], I[32:]
        key = BIP32Key(secret=Il, chain=Ir, depth=0, index=0, fpr='\0\0\0\0', public=False)
        return key

    @staticmethod
    def split_phrase(phrase):
        return phrase.lower().strip().split(' ')

    def get_address_account(self, key):
        AccountZero = key.ChildKey(BIP32_HARDEN)
        ExternalAccount = AccountZero.ChildKey(0)
        address_list = []
        for i in range(self.depth):
            account = dict()
            AddressAccount = ExternalAccount.ChildKey(i)
            account.update({'address': AddressAccount.Address()})
            if self.show_private:
                account.update({'private key': AddressAccount.PrivateKey().encode('hex')})
            address_list.append(account)
        return address_list

    def open_wallet(self, phrase=None) -> list:
        if not phrase:
            phrase = self.pass_phrase
        entropy = self.to_word_array(self.bytes_to_words(self.to_hex(self.make_seed(phrase)))) if self.old \
            else unhexlify(self.to_hex(self.make_seed(phrase)))
        wallet = self.create_wallet(entropy)
        return self.get_address_account(wallet)

    def recover_wallet(self):
        for i in range(1626):
            try_phrase = self.known_phrase
            try_phrase.insert(self.insertion_index, self.mnemonic_word_list[i])
            address_list = list(x['address'] for x in self.open_wallet(try_phrase))
            print(try_phrase)
            print(i, address_list)
            if self.known_address in address_list:
                print(f"FOUND: {self.mnemonic_word_list[i]}")
                break

    def check_phrase(self, partial=False) -> bool:
        if not self.pass_phrase:
            return False
        if not partial and len(self.pass_phrase) != 12 or partial and len(self.pass_phrase) != 11:
            return False
        return all(word in self.mnemonic_word_list for word in self.pass_phrase)

    def decode_base58(self, length=25) -> str:
        n = 0
        for char in self.known_address:
            n = n * 58 + self._b58chars.index(char)
        return (b'\0' * length + int_to_string(n))[-length:]  # n.to_bytes(length, 'big')

    def check_address(self):
        addr_bytes = self.decode_base58()
        addr1 = sha256(bytes(addr_bytes[:-4])).digest()
        addr2 = sha256(addr1).digest()
        chksum = addr2[:4]
        return chksum == addr_bytes[-4:]


def main():
    parser = argparse.ArgumentParser(prog='wallet helper',
                                     description='Offline helper methods for CounterWallet (BIP32).')
    subparsers = parser.add_subparsers(dest='action', help='the action to be taken')

    # recover
    parser_recover = subparsers.add_parser('recover', help='recover a wallet from a partial passphrase')
    parser_recover.add_argument('--known-phrase', required=True, help='enter the part of the passphrase which you know')
    parser_recover.add_argument('--known-address', required=True, help='enter an address known to be in the wallet')
    parser_recover.add_argument('--search-depth', required=False,
                                help='enter the number of generated addresses to search for a match per wallet',
                                type=int, default=3)
    parser_recover.add_argument('--insertion-index', required=True,
                                help='insertion point for unknown word, from 0 to 11', type=int, choices=range(0, 12))
    parser_recover.add_argument('--old', required=False, help='for old 10^32 entropy counterwallets',
                                action='store_true', default=False)

    # wallet
    parser_wallet = subparsers.add_parser('wallet', help='open a wallet, show public or private keys')
    parser_wallet.add_argument('--pass-phrase', required=True, help='enter the wallet passphrase', )
    parser_wallet.add_argument('--search-depth', required=False, help='enter the numberas of addresses to display',
                               type=int, default=3)
    parser_wallet.add_argument('--show-private', required=False, help='show wallet private keys', action='store_true',
                               default=False)
    parser_wallet.add_argument('--old', required=False, help='for old 10^32 entropy counterwallets',
                               action='store_true', default=False)
    args = parser.parse_args()

    checker = CounterWalletHelper(args)

    if args.action == 'wallet':
        try:
            assert checker.check_phrase(args.pass_phrase)
        except AssertionError:
            raise AssertionError('Re-enter a valid 12 word pass phrase or try using the recovery option.')
        for account in checker.open_wallet():
            print(f"address: {account['address']}")
            if args.show_private:
                print(f"private key: {account['private key']}")

    elif args.action == 'recover':
        try:
            assert checker.check_phrase(partial=True)
        except AssertionError:
            InvalidMnemonicException('make sure to enter 11 valid known mnemonic words')
            return
        try:
            assert checker.check_address()
        except AssertionError:
            raise UnknownAddressException('enter a valid known address')
        checker.recover_wallet()


if __name__ == '__main__':
    main()
